use sha1::Digest;
use std::collections::HashMap;
use std::fs;
use std::io;
use std::path::Path;
use std::time::{SystemTime, UNIX_EPOCH};
use serde::Serialize;

#[derive(Debug, Serialize)]
struct FileEntry {
    key: String,
    mime_type: String,
    ctime: u64,
    mtime: u64,
    size: u64,
    inode: Inode,
}

#[derive(Debug, Serialize)]
struct FolderEntry {
    key: String,
    files: Vec<FileEntry>,
    inode: Inode,
}

#[derive(Debug, Serialize)]
enum Entry {
    File(FileEntry),
    Folder(FolderEntry),
}

type Inode = HashMap<String, Entry>;

fn main() {
    let root_path = Path::new("D");

    match index_folder(root_path) {
        Ok(index) => {
            if let Err(e) = write_index_to_file(&index) {
                eprintln!("Error writing index to file: {}", e);
            }
        }
        Err(e) => eprintln!("Error indexing folder: {}", e),
    }
}

fn index_folder(folder_path: &Path) -> io::Result<FolderEntry> {
    let mut folder_entry = FolderEntry {
        key: calculate_folder_key(folder_path)?,
        files: Vec::new(),
        inode: Inode::new(),
    };

    for entry in fs::read_dir(folder_path)? {
        let entry = entry?;
        let path = entry.path();

        if path.is_file() {
            let file_entry = index_file(&path)?;
            folder_entry.files.push(file_entry);
        } else if path.is_dir() {
            let subfolder_entry = index_folder(&path)?;
            folder_entry.inode.insert(subfolder_entry.key.clone(), Entry::Folder(subfolder_entry));
        }
    }

    Ok(folder_entry)
}

fn index_file(file_path: &Path) -> io::Result<FileEntry> {
    let key = calculate_file_key(file_path)?;
    let mime_type = get_mime_type(file_path)?;
    let metadata = fs::metadata(file_path)?;

    let file_entry = FileEntry {
        key,
        mime_type,
        ctime: match metadata.created() {
            Ok(created_time) => created_time.duration_since(UNIX_EPOCH).unwrap().as_secs(),
            Err(_) => {
                // Handle the error, log it, and choose an appropriate fallback value
                eprintln!("Error getting creation time, using fallback");
                SystemTime::now().duration_since(UNIX_EPOCH).unwrap().as_secs()
            }
        },
        mtime: match metadata.modified() {
            Ok(created_time) => created_time.duration_since(UNIX_EPOCH).unwrap().as_secs(),
            Err(_) => {
                // Handle the error, log it, and choose an appropriate fallback value
                eprintln!("Error getting creation time, using fallback");
                SystemTime::now().duration_since(UNIX_EPOCH).unwrap().as_secs()
            }
        },        
        // mtime: metadata.modified().unwrap_or_else(|_| SystemTime::now().duration_since(UNIX_EPOCH).unwrap().as_secs()),
        size: metadata.len(),
        inode: Inode::new(),
    };

    Ok(file_entry)
}

fn calculate_folder_key(folder_path: &Path) -> io::Result<String> {
    Ok(folder_path
        .to_str()
        .ok_or_else(|| io::Error::new(io::ErrorKind::InvalidData, "Invalid folder path"))?
        .to_string())
}

fn calculate_file_key(file_path: &Path) -> io::Result<String> {
    let contents: Vec<u8> = fs::read(file_path)?;
    let hash = sha1::Sha1::digest(&contents);
    Ok(format!("{:x}", hash))
}

fn get_mime_type(file_path: &Path) -> io::Result<String> {
    let mime_type = mime_guess::from_path(file_path)
        .first_or_octet_stream(); // Return "application/octet-stream" if the MIME type cannot be determined

    Ok(mime_type.to_string())
}

fn write_index_to_file(index: &FolderEntry) -> io::Result<()> {
    let index_path = Path::new("D/.baf");
    let index_content = serde_json::to_string_pretty(index)?;

    fs::write(index_path, index_content)?;

    Ok(())
}
